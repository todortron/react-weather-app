# Project Overview
React Weather App takes advantage of the free tier OpenWeatherMap.org API to present 5 day weather forecast both locally and for any city supported by the API.
# Project features
- On loading the user will be prompted to share his location in order to be provided with local forecast. If the prompt is denied, a preset location forecast will be presented. 
- The user is able to search any supported city of interest by typing the city's name and clicking the "Search" button.
- The forecast location will change accordingly to the city of interest.
- The detailed hourly forcast for each day is accessible by clicking on the respective daily card.
- The application will look presentable on smaller screens and mobile.
# Running the project

### `Clone the repo`
    git clone https://gitlab.com/todortron/react-weather-app.git
### `Install dependencies`
    npm i
### `Create OpenWeatherMap account`
    Get an API key from OpenWeatherMap and store it in the .env file
### `Change .env file`
    Change the .env.example file by removing the .example part of the file name, entering the API key and saving it.
### `Run project`
    npm start
# Testing
### `Launch the test runner`
    npm test
## Additional features to think about
- Better card design that accomodates more data.
- Currently the daily card for the current date shows the relevant 3 hour window, but all others show the hourly forecast for 9-12 o'clock which is not exactly a forecast for the entire day. This is due to the limitations of the free tier account that openweathermap supports. This could be resolved by a payed account which offers averaged daily forecast for 16 days. The hourly data could then be mapped to the proper daily average data.
- Theme-based image background on each card.