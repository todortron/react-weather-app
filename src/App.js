import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { FiveDaysSummary, PageNotFound, DailyDetail } from './components';
import { getWeatherData } from './services/getWeatherData';

import './App.scss';

function App() {
  const [weatherData, setWeatherData] = useState();
  const [location, setLocation] = useState();
  const [url, setUrl] = useState();
  const [city, setCity] = useState();

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(success, error, options);
  }, []);

  useEffect(() => {
    if (location) {
      setUrl(
        `https://api.openweathermap.org/data/2.5/forecast?lat=${location.latitude}&lon=${location.longitude}&appid=${process.env.REACT_APP_API_KEY}&units=metric`
      );
    }
  }, [location]);

  useEffect(() => {
    getWeatherData(url, setWeatherData);
    const timer = setInterval(() => {
      getWeatherData(url, setWeatherData);
    }, 10000);
    return () => {
      if (timer) clearInterval(timer);
    };
  }, [url]);

  const options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  const success = (pos) => {
    const crd = pos.coords;
    setLocation({ latitude: crd.latitude, longitude: crd.longitude });
  };

  const error = (err) => {
    setLocation({ latitude: 42.6975, longitude: 23.3242 });
    setCity('Sofia');
    console.warn(`ERROR(${err.code}): ${err.message}`);
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route
          exact
          path='/'
          element={
            <FiveDaysSummary
              weatherData={weatherData}
              location={location}
              setUrl={setUrl}
              setCity={setCity}
              city={city}
            />
          }
        />
        <Route exact path='/daily/:dayId' element={<DailyDetail weatherData={weatherData} city={city} />} />
        <Route path='*' element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
