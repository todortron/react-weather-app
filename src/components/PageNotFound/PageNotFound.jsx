import React from 'react';
import { Link } from 'react-router-dom';

import './PageNotFound.scss';

const PageNotFound = () => {
  return (
    <div className='d-flex justify-content-center align-items-center min-vh-100'>
      <div>
        <h1 className='text-center'>Error 404</h1>
        <h2 className='text-center'>This page doesn&apos;t exist</h2>
        <div className='d-flex justify-content-center'>
          <Link to={`/`} className='btn btn-primary mx-auto'>
            Home
          </Link>
        </div>
      </div>
    </div>
  );
};

export default PageNotFound;
