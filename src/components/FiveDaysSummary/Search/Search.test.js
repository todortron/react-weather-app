import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Search from './Search.jsx';

it('renders correctly', () => {
  render(<Search />);

  expect(screen.getByTestId('search-button')).toBeTruthy();
  expect(screen.getByTestId('search-input')).toBeTruthy();
});

describe('Input value', () => {
  it('updates on change', () => {
    const { getByTestId } = render(<Search />);
    const searchInput = screen.getByTestId('search-input');

    fireEvent.change(searchInput, { target: { value: 'test' } });

    expect(searchInput.value).toBe('test');
  });
});

describe('Search Button', () => {
  describe('with empty query', () => {
    it('does not trigger requestSearch', () => {
      const handleClick = jest.fn();
      const setUrl = jest.fn();
      const setCity = jest.fn();

      render(<Search onClick={handleClick} setUrl={setUrl} setCity={setCity} />);

      const searchButton = screen.getByTestId('search-button');

      fireEvent.click(searchButton);

      expect(handleClick).not.toHaveBeenCalled();
    });
  });

  describe('with data', () => {
    it('triggers requestSearch', () => {
      const requestSearch = jest.fn();
      const setUrl = jest.fn();
      const setCity = jest.fn();

      render(<Search requestSearch={requestSearch} setUrl={setUrl} setCity={setCity} />);

      const searchButton = screen.getByTestId('search-button');
      const searchInput = screen.getByTestId('search-input');

      fireEvent.change(searchInput, { target: { value: 'test' } });

      fireEvent.click(searchButton);

      expect(requestSearch).not.toHaveBeenCalled();
    });
  });
});
