import React, { useState } from 'react';

import './Search.scss';

const Search = ({ setUrl, setCity }) => {
  const [newLocation, setNewLocation] = useState('');

  const handleClick = () => {
    if (!newLocation) {
      return;
    }
    const newUrl = `https://api.openweathermap.org/data/2.5/forecast?q=${newLocation}&appid=${process.env.REACT_APP_API_KEY}&units=metric`;
    setUrl(newUrl);
    setCity(newLocation.charAt(0).toUpperCase() + newLocation.slice(1));
    setNewLocation('');
  };

  return (
    <div className='search-container'>
      <input
        data-testid='search-input'
        type='text'
        name='search-input'
        className='text-input'
        value={newLocation}
        onChange={(e) => setNewLocation(e.target.value)}
      />
      <input type='submit' value='Search' className='search-button' data-testid='search-button' onClick={handleClick} />
    </div>
  );
};

export default Search;
