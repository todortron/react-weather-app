import React from 'react';
import { Link, MemoryRouter as Router } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import DailyCard from './DailyCard.jsx';

it('renders correctly', () => {
  render(
    <Router>
      <DailyCard />
    </Router>
  );

  expect(screen.getByTestId('link')).toBeTruthy();
  expect(screen.getByTestId('card')).toBeTruthy();
  expect(screen.getByTestId('image')).toBeTruthy();
  expect(screen.getByTestId('body')).toBeTruthy();
  expect(screen.getByTestId('temperature')).toBeTruthy();
  expect(screen.getByTestId('feels-like')).toBeTruthy();
  expect(screen.getByTestId('humidity')).toBeTruthy();
  expect(screen.getByTestId('pressure')).toBeTruthy();
});
