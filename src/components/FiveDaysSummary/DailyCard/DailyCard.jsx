import React from 'react';
import Card from 'react-bootstrap/Card';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import { Link } from 'react-router-dom';
import { dayOfTheWeek, normalizedDate } from './../../../helpers/dateConversion';

import './DailyCard.scss';

const DailyCard = ({ id, icon, day, temperature, feelsLike, humidity, pressure }) => {
  return (
    <OverlayTrigger
      placement={'top'}
      overlay={<Tooltip className='over-modal-tooltip'>Click for hourly forecast</Tooltip>}
    >
      <Link className='daily-card-link' to={`/daily/${id}`} data-testid='link'>
        <Card style={{ width: '18rem' }} data-testid='card'>
          <div className='daily-card-date'>
            <div>
              <h2>{dayOfTheWeek(day)}</h2>
            </div>
            <div>
              <h2>{normalizedDate(day)}</h2>
            </div>
          </div>
          <Card.Img variant='top' src={`/weather-icons/${icon}.svg`} data-testid='image' />
          <Card.Body data-testid='body'>
            <Card.Title data-testid='temperature'>{temperature}°C</Card.Title>
            <Card.Text>
              <span className='additional-info' data-testid='feels-like'>
                Feels like: <span className='value-and-unit'>{feelsLike}°C</span>
              </span>
              <span className='additional-info' data-testid='humidity'>
                Humidity: <span className='value-and-unit'>{humidity} %</span>
              </span>
              <span className='additional-info' data-testid='pressure'>
                Pressure: <span className='value-and-unit'>{pressure} hPa</span>
              </span>
            </Card.Text>
          </Card.Body>
        </Card>
      </Link>
    </OverlayTrigger>
  );
};

export default DailyCard;
