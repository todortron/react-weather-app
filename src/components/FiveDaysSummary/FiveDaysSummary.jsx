import React, { useEffect, useState } from 'react';
import { Search, DailyCard } from './';
import Spinner from 'react-bootstrap/Spinner';

import './FiveDaysSummary.scss';

const FiveDaysSummary = ({ weatherData, location, setUrl, city, setCity }) => {
  const [dailyInformation, setDailyInformation] = useState();

  useEffect(() => {
    if (weatherData) {
      produceRelevantInformation(weatherData);
    }
  }, [weatherData]);

  const produceRelevantInformation = (data) => {
    let dailyData = data.map((day, i) => {
      if (i === 0) {
        return { id: day.id, forecast: day.hourly[0] };
      } else if (day.hourly.length > 4) {
        return { id: day.id, forecast: day.hourly[3] };
      }
    });
    if (dailyData.length > 5) {
      const fiveDayData = dailyData.slice(0, 5);
      setDailyInformation(fiveDayData);
    }
  };

  if (weatherData && dailyInformation && location) {
    return (
      <div className='summary-container'>
        <div className='daily-container'>
          <Search setUrl={setUrl} setCity={setCity} data-testid='search' />
          <h1 data-testid='location' className='location'>
            {city ? `Weather in: ${city}` : 'Local Weather Forecast'}
          </h1>
          <div className='daily-gallery-container'>
            <div className='daily-gallery-wrapper'>
              {dailyInformation.map((day, i) => (
                <DailyCard
                  key={i}
                  id={day.id}
                  icon={day.forecast.weather[0].icon}
                  day={day.forecast.day}
                  temperature={Math.round(Number(day.forecast.main.temp))}
                  feelsLike={Math.round(Number(day.forecast.main.feels_like))}
                  humidity={day.forecast.main.humidity}
                  pressure={day.forecast.main.pressure}
                  data-testid='card'
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className='spiner-container'>
        <Spinner animation='border' />
      </div>
    );
  }
};

export default FiveDaysSummary;
