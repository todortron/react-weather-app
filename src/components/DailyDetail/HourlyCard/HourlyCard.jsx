import React from 'react';
import Card from 'react-bootstrap/Card';

import './HourlyCard.scss';

const HourlyCard = ({ hour, icon, temperature, feelsLike, humidity, pressure }) => {
  return (
    <div className='hourly-card'>
      <Card>
        <div className='hourly-card-container'>
          <div className='hourly-card-img-container'>
            <Card.Img variant='top' src={`/weather-icons/${icon}.svg`} />
          </div>
          <div className='hourly-card-body-container'>
            <Card.Header as='h5'>
              <span>Hour:</span>
              <div>
                <span>{hour}</span>
              </div>
            </Card.Header>
            <Card.Body>
              <Card.Title>Teperature: {temperature}°C</Card.Title>
              <Card.Text>
                <span className='additional-info'>
                  Feels like: <span className='value-and-unit'>{feelsLike}°C</span>
                </span>
                <span className='additional-info'>
                  Humidity: <span className='value-and-unit'>{humidity} %</span>
                </span>
                <span className='additional-info'>
                  Pressure: <span className='value-and-unit'>{pressure} hPa</span>
                </span>
              </Card.Text>
            </Card.Body>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default HourlyCard;
