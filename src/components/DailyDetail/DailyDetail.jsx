import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import HourlyCard from './HourlyCard/HourlyCard';
import { normalizedDate } from './../../helpers/dateConversion';
import Spinner from 'react-bootstrap/Spinner';

import './DailyDetail.scss';

const DailyDetail = ({ weatherData, city }) => {
  const navigate = useNavigate();
  const { dayId } = useParams();
  const [oneDayData, setOneDayData] = useState();

  useEffect(() => {
    setOneDayData(getRelevantDayData());
  }, [weatherData]);

  useEffect(() => {
    if (weatherData) {
      const ids = weatherData.map((day) => day.id);
      if (!ids.some((id) => id === dayId)) {
        navigate('/404');
      }
    }
  }, [dayId, weatherData]);

  const getRelevantDayData = () => {
    if (weatherData) {
      const dailyData = weatherData.find((day) => day.id === dayId);
      return dailyData;
    }
  };

  const transformHour = (date) => {
    let properHour;
    if (date.slice(11, 16)[0] === '0' && date.slice(11, 16)[1] === '0') {
      properHour = date.slice(11, 16);
    } else if (date.slice(11, 16)[0] === '0') {
      properHour = date.slice(12, 16);
    } else {
      properHour = date.slice(11, 16);
    }
    return properHour;
  };

  if (weatherData && oneDayData) {
    return (
      <div className='daily-wrapper'>
        <div className='daily-container'>
          <h1>
            {city
              ? `Weather in ${city} for ${normalizedDate(oneDayData.id)}`
              : `Local weather for ${normalizedDate(oneDayData.id)}`}
          </h1>
          {oneDayData.hourly.map((hourly, i) => (
            <HourlyCard
              key={i}
              hour={transformHour(hourly.dt_txt)}
              icon={hourly.weather[0].icon}
              temperature={Math.round(Number(hourly.main.temp))}
              feelsLike={Math.round(Number(hourly.main.feels_like))}
              humidity={hourly.main.humidity}
              pressure={hourly.main.pressure}
            />
          ))}
        </div>
      </div>
    );
  } else {
    return (
      <div className='spiner-container'>
        <Spinner animation='border' />
      </div>
    );
  }
};

export default DailyDetail;
