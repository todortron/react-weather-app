import axios from 'axios';

export const fetchData = async (url) => {
  return axios.get(url).then((res) => {
    switch (res.status) {
      case 200:
        return res.data;
      default:
        throw new Error(res.data.error);
    }
  });
};
