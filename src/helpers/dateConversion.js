export const dayOfTheWeek = (day) => {
  const weekday = new Date(day).toLocaleDateString('en', {
    weekday: 'long'
  });
  return weekday;
};

export const normalizedDate = (day) => {
  const date = new Date(day).toLocaleDateString('en', {
    year: 'numeric',
    month: 'short',
    day: 'numeric'
  });
  return date;
};
