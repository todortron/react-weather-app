import { fetchData } from '../helpers/fetchData';

const transformWeatherData = async (url) => {
  const rawWeatherData = await fetchData(url);
  // Introduces day property
  const introducedDates = rawWeatherData.list.map((reading) => ({
    ...reading,
    day: reading.dt_txt.substring(0, 10)
  }));
  // Groups hourly data by day
  const groupedDates = groupBy(introducedDates, 'day');
  const relevantDates = Object.keys(groupedDates).map((key) => {
    return { id: key, hourly: groupedDates[key] };
  });
  return relevantDates;
};

const groupBy = (array, key) => {
  return array.reduce((rv, x) => {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export const getWeatherData = async (url, setStateFunction) => {
  const data = await transformWeatherData(url);
  setStateFunction(data);
};
